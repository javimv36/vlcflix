import requests
from bs4 import BeautifulSoup
from subprocess import call
urls = ["http://www.elitetorrent.biz", "http://www.mejortorrent1.com", "https://grantorrent.net", "https://grantorrent.net/series"]

def searchElitetorrent(search, url):
    search=search.replace(" ", "+")
    pagina=1
    while True:
        page = requests.get(url+"/page/"+str(pagina)+"/?s="+search)
        soup = BeautifulSoup(page.content, 'html.parser')
        # print(soup.prettify())
        html=list(soup.children)[2]
        #header=list(html.children)[1]
        body=list(html.children)[3]
        #print(body)
        resultados=body.find_all('div', class_='imagen')
        cuenta=0
        for resultado in resultados:
            print(str(cuenta) + " : " + str(list(resultado.children)[1]['alt']) + "\n")
            cuenta+=1
        indice = input("Cuál quieres ver? (escribe N si NO está en la lista): ")
        if(indice == "N" or indice == "n"):
            pagina+=1        
        else:
            print (list(resultados[int(indice)].children)[1]['alt'])
            break
    torrent= requests.get(list(resultados[int(indice)].children)[1]['href'])
    soup = BeautifulSoup(torrent.content, 'html.parser')
    # print(soup.prettify())
    html=list(soup.children)[2]
    body=list(html.children)[3]
    link= body.find(class_='enlace_torrent')
    torrent_url=url+link['href']
   
    return torrent_url

def searchMejortorrent(search, url):
    search=search.replace(" ", "%20")
    pagina=1
    while True:
        #https://mejortorrent1.com/buscador/page/2/?search=juego%20tronos
        page = requests.get(url+"/buscador/page/"+str(pagina)+"/?search="+search)
        soup = BeautifulSoup(page.content, 'html.parser')
        # print(soup.prettify())
        html=list(soup.children)[2]
        body=list(html.children)[3]
        #print(body)
        resultados=body.find_all('a', class_='titulo_buscador')
        cuenta=0
        for resultado in resultados:
            # print(resultado.get_text())
            # print(resultado["href"])
            print(str(cuenta) + " : " + resultado.get_text() + "\n")
            cuenta+=1
        indice = input("Cuál quieres ver? (escribe N si NO está en la lista): ")
        if(indice == "N" or indice == "n"):
            pagina+=1        
        else:
            #print (list(resultados[int(indice)].children)[1]['alt'])
            break
    torrent= requests.get(resultados[int(indice)]['href'])
    soup = BeautifulSoup(torrent.content, 'html.parser')
    # print(soup.prettify())
    html=list(soup.children)[2]
    body=list(html.children)[3]
    link= body.find(class_='enlace_torrent')
    torrent_url=url+link['href']

    return ""

def searchGrantorrent(search, url):
    #https://grantorrent.net/series/page/1/?s=el
    search=search.replace(" ", "+")
    pagina=1
    while True:
        page = requests.get(url+"/page/"+str(pagina)+"/?s="+search)
        soup = BeautifulSoup(page.content, 'html.parser')
        #print(soup.prettify())
        html=list(soup.children)[2]
        #print(len(list(html.children)))
        #header=list(html.children)[1]
        body=list(html.children)[9]
        print(body)
        resultados=body.find_all('div', class_='imagen-post')
        cuenta=0
        for resultado in resultados:
            # print(resultado.get_text())
            # print(resultado["href"])
            print(list(resultado.children)[1]['href'])
            print(str(cuenta) + " : " + list(resultado.children)[3].get_text() + "\n")
            cuenta+=1
        indice = input("Cuál quieres ver? (escribe N si NO está en la lista): ")
        if(indice == "N" or indice == "n"):
            pagina+=1        
        else:
            #print (list(resultados[int(indice)].children)[1]['alt'])
            break
        print(list(resultados.children))
    return ""

def searchGrantorrentSeries(search, url):
    return ""

for u in urls:
    print(str(urls.index(u)) + " " + u)
service = input("Qué web quieres utilizar?: ")
search = input("Qué quieres ver?: ")

if(service=="0"):
    magnet = searchElitetorrent(search, urls[0])
elif(service=="1"):
    magnet = searchMejortorrent(search, urls[1])
elif(service=="2"):
    magnet = searchGrantorrent(search, urls[2])
elif(service=="3"):
    magnet = searchGrantorrentSeries(search, urls[3])
else:
    magnet=""

print (magnet)
call(["peerflix", magnet , "-q", "--vlc" , "--", "--qt-minimal-view", "-f"], shell=False)
